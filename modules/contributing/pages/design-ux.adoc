= Design & UX of docs.fedoraproject.org site
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>

This page explains how to contribute to the Fedora Docs Team as a designer or user interface (U.I.) / user experience (U.X.) developer.

https://pagure.io/fedora-docs/fedora-docs-ui[*fedora-docs-ui*]::
Sources of a U.I. for the new `docs.fedoraproject.org` site.

https://docs.antora.org/antora-ui-default/build-preview-ui/[*Build a UI Project for Local Preview*]::
Official `antora.org` documentation on how to build a preview site using a custom U.I.
