= Introduction
This document explains how to work with the new publishing system used to build the Fedora Documentation website and others. It will guide you through contributing to existing documentation as well as creating completely new content sets and publishing them in the English originals as well as any possible translations.

This guide explains Antora in the context of the Fedora Documentation website. For more general information about the Antora publishing system, see the https://antora.org/[Antora website] and https://docs.antora.org/antora/latest/page/[Antora documentation].
