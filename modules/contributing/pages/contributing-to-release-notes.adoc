include::ROOT:partial$attributes.adoc[]

= Contributing to Release Notes

This section describes how to contribute to Fedora Release Notes.
Before you start following this procedure, make sure that you fulfill all the requirements listed in xref:prerequisites.adoc[Prerequisites].

[TIP]
The Release Notes contribution process is on a separate page because it is different from any other Fedora documentation.
If you are interested in contributing to any other docs, see the xref:contributing-to-existing-docs.adoc[appropriate page].

The Release Notes are unique in that, unlike all other documentation, they are written nearly completely from scratch for each new release, while all other docs are being kept between releases, only with updates.
This means that some parts of the procedure below, such as the git branch used or the schedule, change with each new release.
Before you start writing release notes, see the link:https://discussion.fedoraproject.org/tag/docs[`#docs` tag on Fedora Discussion], speficically the sidebar on the right.
You will find current information there.

== How to contribute to Fedora Release Notes

. Check the sidebar on link:https://discussion.fedoraproject.org/tag/docs[Fedora Discussion], and note the provided information: Release, schedule, branch, and link to the list of relevant issues.

. Pick one or more issues from the list of open and unassigned issues for the release which you want to contribute to.

. Open each issue you picked, and click the Take button on the right to claim it.
+
[IMPORTANT]
If you claim an issue, but later find out you don’t have time to finish it, remove yourself from the issue ASAP so someone else can pick it up.

. Find information about your issue.
A lot of them have plenty of info in them already, especially in the Wiki link; if you need more, find out who is responsible for the change (also listed on the Wiki page linked in the issue), and @-mention them in the issue comments or talk to them on IRC/Matrix or via mail.
Keep in mind that it’s always better if you try to do research before you ask questions.
Note that you might not always be able to reach the owner in a reasonable time frame; in that case just do your best, if something ends up being wrong, we can always update it later.

. Write a release note about the issue. If you’re not sure what exactly a release note looks like, check out some of the link:https://docs.fedoraproject.org/en-US/fedora/latest/release-notes/[previous releases] for inspiration.
We don’t want any long, overly technical texts, the release notes are generally meant to highlight changes, not to tell people how to use something.

. Now the workflow diverges based on your Pagure permissions and technical skills:
+
* If you know how to use git and ASCIIDoc, write up the release note and send a pull request against the main repository. Make sure you open the pull request against the correct branch, not `main` - see first step of this procedure for info.
(The `main` branch is only used as a template for each new release, so it only contains an empty structure and some pages that do not vary between releases.)
Your contributions should go into one of the files in `modules/release-notes/pages/`, which one exactly depends on the contents
of the change you’re documenting.
Use the `build.sh` and `preview.sh` scripts in the repository root to preview your changes locally; see the link:https://docs.fedoraproject.org/en-US/fedora-docs/contributing/local-preview/[docs] for specific instructions.
If you can’t see the section where you added your contributions at all, make sure that it’s included in the table of contents in `modules/release-notes/nav.adoc`.
+
* If the above sounds like gibberish to you, it’s fine. Just add a comment with your text into the issue, and someone will mark it up and add it to the final document.

Repeat the above for as many issues as you want.

See also the xref:git.adoc[Git for docs writers] page for more detailed information about working with Fedora Documentation git repositories.

If you have any questions, xref:ROOT:index.adoc#find-docs[contact Docs on IRC, Matrix, or the forums].
